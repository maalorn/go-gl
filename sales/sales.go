package sales

import (
	"fmt"
	"go-gl/ledger"
	"time"
)

type Sale struct {
	SaleDate         time.Time
	Price            float64
	TaxRate          float64
	TaxAmount        float64
	TitleFee         float64
	DealerServiceFee float64
	CashPrice        float64
}

type SaleEntry struct {
	*Sale
	DateAdded time.Time
}

type Sales []*SaleEntry

var sales Sales

func init() {
	sales = make(Sales, 0)
}

func New(date time.Time, price, tax, title, service float64) *Sale {
	s := &Sale{
		date,
		price,
		tax,
		0.0,
		title,
		service,
		0.0,
	}

	s.Recalculate()

	return s
}

func (s *Sale) Recalculate() {
	taxAmount := (s.Price + s.DealerServiceFee) * s.TaxRate
	cashPrice := s.Price + s.DealerServiceFee + s.TitleFee + s.TaxAmount
	s.TaxAmount, s.CashPrice = taxAmount, cashPrice
}

func Insert(s *Sale) *Sale {
	fmt.Printf("%#v\n", s)
	s.Recalculate()
	fmt.Printf("%#v\n", s)

	sales = append(sales, &SaleEntry{
		s,
		time.Now(),
	})

	// This is obviously incorrect unless selling at cost is a thing everybody does
	// 100% of the time. Who cares.
	UpdateLedger(s)

	return s
}

func UpdateLedger(s *Sale) {
	ledger.Insert(ledger.Entry{s.SaleDate, 3500, "", 0, s.Price})
	ledger.Insert(ledger.Entry{s.SaleDate, 2240, "", 0, s.TaxAmount})
	ledger.Insert(ledger.Entry{s.SaleDate, 9070, "", 0, s.DealerServiceFee})
	ledger.Insert(ledger.Entry{s.SaleDate, 2030, "", 0, s.TitleFee})
	ledger.Insert(ledger.Entry{s.SaleDate, 1010, "", s.Price + s.TaxAmount + s.DealerServiceFee + s.TitleFee, 0})
	ledger.Insert(ledger.Entry{s.SaleDate, 1450, "", 0, s.Price})
	ledger.Insert(ledger.Entry{s.SaleDate, 4500, "", s.Price, 0})
}

func Delete(id int64) (*Sale, error) {
	if id < 0 || id > int64(len(sales)) {
		return nil, fmt.Errorf("Sales ID out of range. %d not in [0:%d]", id, len(sales))
	}

	if sales[id] == nil {
		return nil, fmt.Errorf("No sale with ID %d", id)
	}

	s := *sales[id]
	sales[id] = nil

	rev := *s.Sale
	UpdateLedger(Reversal(&rev))

	return s.Sale, nil
}

func Reversal(s *Sale) *Sale {
	s.Price = -s.Price
	s.TaxAmount = -s.TaxAmount
	s.TitleFee = -s.TitleFee
	s.DealerServiceFee = -s.DealerServiceFee
	s.CashPrice = -s.CashPrice

	return s
}

func Listing() string {
	var s string
	var totals Sale

	s += fmt.Sprintf(
		"%-9.9s   %-11.11s  %-8.8s  %-9.9s  %-7.7s  %-10.10s\n",
		"Sale Date",
		"   Price   ",
		" Taxes  ",
		"Title Fee",
		"  DSF  ",
		"Cash Price",
	)

	s += fmt.Sprintf(
		"%-9.9s   %-11.11s  %-8.8s  %-9.9s  %-7.7s  %-10.10s\n",
		"---------",
		"-----------",
		"---------",
		"---------",
		"-------",
		"----------",
	)

	for _, sale := range sales {
		if sale != nil {
			s += fmt.Sprintf(
				"%-9.9s   %11.2f  %8.2f  %9.2f  %7.2f  %10.2f\n",
				sale.SaleDate.Format("01/02/06"),
				sale.Price,
				sale.TaxAmount,
				sale.TitleFee,
				sale.DealerServiceFee,
				sale.CashPrice,
			)
			totals.Price += sale.Price
			totals.TaxAmount += sale.TaxAmount
			totals.TitleFee += sale.TitleFee
			totals.DealerServiceFee += sale.DealerServiceFee
			totals.CashPrice += sale.CashPrice
		}
	}

	s += fmt.Sprintf(
		"%-9.9s   %-11.11s  %-8.8s  %-9.9s  %-7.7s  %-10.10s\n",
		"",
		"===========",
		"========",
		"=========",
		"=======",
		"==========",
	)

	s += fmt.Sprintf(
		"%-9.9s   %11.2f  %6.2f  %9.2f  %7.2f  %10.2f\n",
		"",
		totals.Price,
		totals.TaxAmount,
		totals.TitleFee,
		totals.DealerServiceFee,
		totals.CashPrice,
	)
	return s
}
