package main

import (
	"fmt"
	"go-gl/ledger"
	"go-gl/sales"
	"time"
)

func main() {
	s := sales.New(
		time.Now().Add(-5*24*time.Hour),
		1000.00,
		0.04,
		10.00,
		49.00,
	)
	sales.Insert(s)

	s = nil
	s = sales.New(
		time.Now(),
		10000.00,
		0.05,
		100.00,
		250.00,
	)
	sales.Insert(s)

	fmt.Println(sales.Listing())
	fmt.Println(ledger.Listing())

	d, err := sales.Delete(0)
	if err != nil {
		panic(err)
	}

	d.Price = 1000000.00
	sales.Insert(d)

	fmt.Println(sales.Listing())
	fmt.Println(ledger.Listing())
	fmt.Println(ledger.BalanceSheet())
	fmt.Println(0xBadFace)
}
