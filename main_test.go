package main

import (
	"go-gl/ledger"
	"go-gl/sales"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSaleGL(t *testing.T) {
	s := sales.New(
		time.Now().Add(-5*24*time.Hour),
		1000.00,
		0.05,
		10.00,
		49.00,
	)
	ns := sales.Insert(s)
	debits, credits := ledger.Balance()

	Convey("Given a completed sale", t, func() {
		Convey("The GL should balance to zero", func() {
			if debits != credits {
				t.Errorf("GL not in balance. Debits ($%.2f) ! = Credits ($%.2f).", debits, credits)
			}
		})

		Convey("The sum of debits should equal the cash price", func() {
			if debits != ns.CashPrice+ns.Price {
				t.Errorf("Expecting debits = $%.2f, got $%.2f.", ns.CashPrice, debits)
			}
		})

		Convey("The sum of credits should equal the cash price", func() {
			if credits != ns.CashPrice+ns.Price {
				t.Errorf("Expecting credits = $%.2f, got $%.2f.", ns.CashPrice, credits)
			}
		})

		Convey("When the sale is deleted", func() {
			sales.Delete(0)

			debitsNew, creditsNew := ledger.Balance()
			debitsNew, creditsNew = debitsNew-debits, creditsNew-credits

			Convey("The GL should balance to zero", func() {
				if debits != credits {
					t.Errorf("GL not in balance. Debits ($%.2f) ! = Credits ($%.2f).", debits, credits)
				}
			})

			Convey("The balance for all accounts should be zero", func() {
				for k, v := range ledger.AccountBalances() {
					if v != 0 {
						t.Errorf("Expecting balance for account %d to be $%.2f, got $%.2f", k, 0.0, v)
					}
				}
			})
		})
	})
}
