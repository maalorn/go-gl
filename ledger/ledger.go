package ledger

import (
	"fmt"
	"time"
)

var (
	gl Ledger
)

func init() {
	gl = make(Ledger, 0)
}

type Entry struct {
	Date        time.Time
	Account     int64
	Description string
	Debit       float64
	Credit      float64
}

type Ledger []Entry

func Listing() string {
	var dTotal, cTotal float64
	var s string = fmt.Sprintf("%-8.8s %7.7s %-33.33s %12.12s %12.12s\n", "Date", "Account", "Description", "Debit", "Credit")

	s += fmt.Sprintf(
		"%8.8s %7.7s %-33.33s %12.12s %12.12s\n",
		"--------", "-------", "---------------------------------", "------------", "------------",
	)

	for _, line := range gl {
		dTotal += line.Debit
		cTotal += line.Credit
		s += fmt.Sprintf(
			"%s %-7d %-33.33s %12.2f %12.2f\n",
			line.Date.Format("01/02/06"), line.Account, line.Description, line.Debit, line.Credit,
		)
	}

	s += fmt.Sprintf("%50.50s %12.12s %12.12s\n", "", "============", "============")
	s += fmt.Sprintf("%50.50s %12.2f %12.2f\n", "", dTotal, cTotal)
	return s
}

func Insert(e Entry) Entry {
	var ok bool
	e.Description, ok = chart.Description(e.Account)
	if !ok {
		e.Description = "Undefined Account"
	}

	if e.Debit != 0 && e.Debit < 0 {
		e.Credit = -e.Debit
		e.Debit = 0
	}

	if e.Credit != 0 && e.Credit < 0 {
		e.Debit = -e.Credit
		e.Credit = 0
	}

	gl = append(gl, e)
	return e
}

func Balance() (debits, credits float64) {
	for _, line := range gl {
		debits += line.Debit
		credits += line.Credit
	}
	return
}

func AccountBalances() map[int64]float64 {
	m := make(map[int64]float64)
	for _, v := range gl {
		m[v.Account] += v.Debit
		m[v.Account] -= v.Credit
	}
	return m
}

func BalanceSheet() string {
	var assets, liabilities, costSales, expenses float64
	bals := AccountBalances()

	for num, amt := range bals {
		switch {
		case 1000 <= num && num < 2000:
			assets += amt
		case 2000 <= num && num < 3000:
			liabilities -= amt
		case 3000 <= num && num < 5000:
			costSales -= amt
		case 8000 <= num && num < 9000:
			expenses += amt
		case 9000 <= num && num < 10000:
			costSales -= amt
		}
	}

	equity := costSales - expenses

	return fmt.Sprintf(
		"Assets\n"+
			"------------\n"+
			"%12.2f\n\n"+
			"Liabilities\n"+
			"------------\n"+
			"%12.2f\n\n"+
			"Equity\n"+
			"------------\n"+
			"%12.2f\n\n"+
			"============\n"+
			"%12.2f\n",
		assets, liabilities, equity, assets-liabilities-equity,
	)
}
