package ledger

type Chart map[int64]string

var chart Chart

func init() {
	chart = make(map[int64]string)
	chart[1010] = "Undeposited Funds"
	chart[1450] = "Inventory"

	chart[2030] = "License & Registration"
	chart[2240] = "Sales Tax - State"

	chart[3500] = "Sales - Retail"

	chart[4500] = "Cost of Sales"

	chart[9070] = "Other Income - Dealer Service Fee"
}

func (c *Chart) Description(num int64) (string, bool) {
	v, ok := chart[num]
	return v, ok
}
